import React, { useState } from 'react';
import './App.css';
import AppApp from './components/AppApp';
import InputNumber from './components/InputNumber';
import Total from './components/Total';

function App () {
	const [ count, setCount ] = useState(0);

	const [ auth, setAuth ] = useState({
		username: '',
		password: ''
	});

	// useState structure
	// const [value, setValue] = useState(initialState)

	// data type
	// {} object
	// [] array

	// object
	// -> properties

	// function
	// {} = multiple line // return required
	// () = return

	const handleChange = (e) => setCount(e.target);

	return (
		<AppApp title='Basic'>
			<InputNumber value={num1} handleChange={handleChange} />
			<InputNumber value={num2} handleChange={handleChange} />
			<br />
			<Total displayCount={count} />
		</AppApp>
	);
}

const Button = () => {
	return <Button>Button</Button>;
};

const Button = () => <Button>Button</Button>;

export default App;
