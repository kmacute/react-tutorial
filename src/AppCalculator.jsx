import React, { useState } from 'react';
import './App.css';

function App () {
	// dim count as integer = 0 // vb
	const [ count, setCount ] = useState(0); // React

	function handleClick () {
		// setCount(count + 1); // Approach 1
		setCount((prev) => {
			alert(1);
		});
	}

	return (
		<div className='App'>
			<button onClick={() => handleClick()}>Click me {count}</button>
		</div>
	);
}

export default App;
