import React from 'react';

const InputNumber = ({ value, handleChange }) => {
	return <input type='number' value={value} onChange={handleChange} />;
};

export default InputNumber;
