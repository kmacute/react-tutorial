import React from 'react';

const Total = ({ displayCount = 0 }) => {
	return <span>Total {displayCount}</span>;
};

export default Total;
