import React from 'react';

const AppApp = ({ children, title }) => {
	return (
		<div className='App'>
			<h1>{title}</h1>
			{children}
		</div>
	);
};

export default AppApp;
